package se.chalmers.cse.wm1819.dit341template.model;

public class Comment {
    public   String id;
    public   String author;
    public   String text;

    public Comment(String id, String author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
    }
}
