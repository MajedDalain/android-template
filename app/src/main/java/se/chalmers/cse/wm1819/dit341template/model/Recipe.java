package se.chalmers.cse.wm1819.dit341template.model;

import org.json.JSONArray;

public class Recipe {

    private String title;
    private String imageUrl;
    private String description;
    private String RecipeId;
    private JSONArray comments;


    public void setComments(JSONArray comments) {
        this.comments = comments;
    }

    public Recipe(String title, String description, String image, String id, JSONArray comments) {
        this.title = title;
        this.description = description;
        this.imageUrl= image;
        this.RecipeId = id;
        this.comments = comments;
    }

    public String getTitle() {
        return title;
    }


    public String getImageUrl() {
        return imageUrl;
    }
    public String getDescription() {
        return description;
    }

    public String getRecipeId() {
        return RecipeId;
    }

    public JSONArray getComments() {
        return comments;
    }
}

