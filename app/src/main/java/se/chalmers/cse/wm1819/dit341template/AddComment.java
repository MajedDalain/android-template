package se.chalmers.cse.wm1819.dit341template;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddComment extends AppCompatActivity {

    private  static String recipeID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcomment);

//         recipeID = getIntent().getStringExtra("id");

        //TextView text = findViewById(R.id.displayTextView);
        // text.setText("Text from my main activity: " + message);
    }

    public void onClickSubmitComment(View view) {

        TextView author = findViewById(R.id.AuthorText);
        TextView comment = findViewById(R.id.CommentText);

        JSONObject newJSONNote = new JSONObject();
        try {
            newJSONNote.put("author", author.getText().toString());
            newJSONNote.put("text", comment.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(this);




        String url = "http://10.0.2.2:3000/user/recipes/" + recipeID +"/comments";
        JsonObjectRequest newComment = new JsonObjectRequest
                (Request.Method.POST, url, newJSONNote, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("done");
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString());
                    }

                }){
            @Override
            public Map<String, String> getHeaders() {
                final Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        queue.add(newComment);
    }

}
