package se.chalmers.cse.wm1819.dit341template;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;
import java.util.zip.Inflater;

import se.chalmers.cse.wm1819.dit341template.model.Recipe;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>{

    private Context context;
    private List<Recipe> recipelist;

    public RecipeAdapter(Context context, List<Recipe> recipelist) {
        this.context = context;
        this.recipelist = recipelist;
    }


    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item,parent,false);
        return  new RecipeViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder recipeViewHolder, int i) {

        Recipe recipe = recipelist.get(i);

        recipeViewHolder.textViewHead.setText(recipe.getTitle());
        recipeViewHolder.textViewDesc.setText(recipe.getDescription());

        Picasso.get().load(recipe.getImageUrl()).into(recipeViewHolder.imageView);

        final Intent intent = new Intent(context, GetCommentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("image_url",recipe.getImageUrl());
        intent.putExtra("recipe_title",recipe.getTitle());
        intent.putExtra("id",recipe.getRecipeId());
        intent.putExtra("comments",recipe.getComments().toString());

        recipeViewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {

        return recipelist.size();
    }

    public class RecipeViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewHead;
        public TextView textViewDesc;
        public ImageView imageView;



        public RecipeViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewHead = (TextView) itemView.findViewById(R.id.textViewHeading);
            textViewDesc = (TextView) itemView.findViewById(R.id.textViewDesc);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);


        }
    }
}
