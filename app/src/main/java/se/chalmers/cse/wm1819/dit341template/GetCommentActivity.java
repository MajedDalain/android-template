package se.chalmers.cse.wm1819.dit341template;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import se.chalmers.cse.wm1819.dit341template.model.Comment;
import se.chalmers.cse.wm1819.dit341template.model.Recipe;

public class GetCommentActivity extends AppCompatActivity {
    private static final String TAG = "GetCommentActivity";
    private  String recipeId , imageURl;
    private   String URL_DATA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getcomment);
        Log.d(TAG,"onCreate: started.");

        getIncomingIntent();
        setRecipe(imageURl);
        loadComments();


        Button btnAddComment = (Button)findViewById(R.id.btnAddComment);

       final  Intent intent = new Intent(this,AddComment.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id", recipeId);

        btnAddComment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intent);
            }
        });


    }

    private void getIncomingIntent(){
        Log.d(TAG,"getIncomingIntent: checking for incoming intent ");
        if(getIntent().hasExtra("image_url")&& getIntent().hasExtra("recipe_title")){
            imageURl = getIntent().getStringExtra("image_url");
            recipeId = getIntent().getStringExtra("id");
        }

    }

    private void setRecipe(String imageUrl){
        Log.d(TAG,"setRecipe: setting the image and title");
        ImageView imageView = findViewById(R.id.image);
        Picasso.get().load(imageUrl).into(imageView);
    }


    private void loadComments(){
        final TextView commentsView = findViewById(R.id.recipe_comments);
        URL_DATA =  "http://10.0.2.2:3000/admin/recipes/"+ recipeId +"/comments/";
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Comments....");
        progressDialog.show();



        //This uses Volley (Threading and a request queue is automatically handled in the background)
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, URL_DATA, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        //GSON allows to parse a JSON string/JSONObject directly into a user-defined class
                        Gson gson = new Gson();

                        String dataArray = null;

                        try {
                            dataArray = response.getString("comments");
                        } catch (JSONException e) {
                            Log.e(this.getClass().toString(), e.getMessage());
                        }

                        StringBuilder commentString = new StringBuilder();
                        commentString.append("This is the list of comments: \n");

                        Comment[] comments = gson.fromJson(dataArray, Comment[].class);

                        for (Comment current : comments) {
                            commentString.append("comment by " + current.author + " is "
                                    + current.text + "\n");
                        }

                        commentsView.setText(commentString.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        commentsView.setText("Error! " + error.toString());
                    }
                });

        //The request queue makes sure that HTTP requests are processed in the right order.
        queue.add(jsonObjectRequest);
    }


}
