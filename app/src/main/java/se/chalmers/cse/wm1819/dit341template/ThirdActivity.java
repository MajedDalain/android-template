package se.chalmers.cse.wm1819.dit341template;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ThirdActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createrecipe);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.HTTP_PARAM);

        //TextView text = findViewById(R.id.displayTextView);
       // text.setText("Text from my main activity: " + message);
    }

    public void onClickSubmitRecipe(View view) {


        // this is the data the user inputs in the textfield view
        TextView title = findViewById(R.id.titleText);
        TextView image = findViewById(R.id.imageURLtext);
        TextView description = findViewById(R.id.descriptionText);


        JSONObject newJSONNote = new JSONObject();
        try {
            newJSONNote.put("title", title.getText().toString());
            newJSONNote.put("image", image.getText().toString());
            newJSONNote.put("description", description.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(this);

        String url = "http://10.0.2.2:3000/user/recipes";
        JsonObjectRequest newRecipe = new JsonObjectRequest
                (Request.Method.POST, url, newJSONNote, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("done");
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString());
                    }

                }){
            @Override
            public Map<String, String> getHeaders() {
                final Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        queue.add(newRecipe);

        // finish this activity
        finish();

    }
}
