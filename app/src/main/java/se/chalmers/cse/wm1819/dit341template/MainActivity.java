package se.chalmers.cse.wm1819.dit341template;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    public static final String HTTP_PARAM = "httpResponse";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnGetAll = (Button)findViewById(R.id.btnGetAll);
        Button btnAddRecipe = (Button)findViewById(R.id.btnAddRecipe);

        btnGetAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,  RecipeList.class));
            }
        });


        btnAddRecipe.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,  ThirdActivity.class));
            }
        });

    }


}




